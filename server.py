#!/usr/bin/env python

import os
import re
import os.path
try:
    # This works for Python 2
    from SimpleHTTPServer import SimpleHTTPRequestHandler
    import SimpleHTTPServer
except ImportError:
    # This works for Python 3
    from http.server import SimpleHTTPRequestHandler
    import http.server as SimpleHTTPServer

BASE_PATH="./"

class Includer(SimpleHTTPRequestHandler):
    # Adds minimal support for <!-- #include --> directives.

    def __init__(self, request, client_address, server):
        SimpleHTTPRequestHandler.__init__(self, request, client_address, server)

    def do_GET(self):
            if os.path.exists(BASE_PATH + self.path):
                self.protocol_version='HTTP/1.1'
                self.send_response(200, 'OK')
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(self.generate_content(BASE_PATH + self.path))
            else:
                self.protocol_version='HTTP/1.1'
                self.send_response(404, 'Not Found')
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write("No such file")

    def do_HEAD(self):
        SimpleHTTPRequestHandler.do_HEAD(self)

    def generate_content(self, file):
        content = ""
        with open(file, 'r') as f:
            for line in f:
                match = re.search(r'<!-- *include *[\'"]([^\'"]+)[\'"] *-->', line)
                if match != None:
                    include_file = BASE_PATH + match.group(1)
                    print("Found include: %s" % include_file)
                    if os.path.exists(include_file):
                        print("Adding content from %s" % include_file)
                        content += self.generate_content(include_file)
                    else:
                        print("File %s not found" % include_file)
                        continue
                else:
                    content += line
        return content

if __name__ == '__main__':
    SimpleHTTPServer.test(HandlerClass=Includer)
# Web server to serve RemarkJS.

## Problem statement
[RemarkJS](https://remarkjs.com) is an awesome and easily extendable presentation engine. It also support Markdown formated slides to be included as an external file by using option `sourceUrl` like in following example

```html
<script>
    var slideshow = remark.create({
        ratio: "16:9",
        sourceUrl: 'slides.md',
        highlightStyle: 'monokai',
        countIncrementalSlides: true,
        slideNumberFormat: ''
    });
</script>
```

However with that type of configuration it is hard to use RemarkJS plugins like [Mermaid Graphs](https://mermaidjs.github.io/) which expect markdown presentation available within page source.

## Solution

This project provides rather simple HTTP server based on Python default HTTP library, which replace all tags

```
<!-- include "path/to/file" -->
```

with actuall file content on fly. If there are no file available this tag will be replaced by empty string.

This approach provides flexibility of having markdown slides as sepatate file with all RemarkJS plugins work normally.

The server also support multy-level inclusion, however does not detect loops.